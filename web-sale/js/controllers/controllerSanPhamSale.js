export let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
export let offLoading = () => {
  document.getElementById("loading").style.display = "none";
};
//luu dsnv xuong localStorage
export let saveLocalStorage = (LISTCART, listCart) => {
  let listCartJson = JSON.stringify(listCart);
  localStorage.setItem(LISTCART, listCartJson);
  console.log("localStorage: ", localStorage);
};

export let renderThongTinSanPham = (list) => {
  let contentHTML = "";
  list.forEach((sanPham) => {
    contentHTML += `
    <div class="col-md-3 my-3">
    <div class="card-sl">
      <div class="card-image">
        <img src="${sanPham.hinhAnh}" />
      </div>
      <a class="card-action" href="#"><i class="fa fa-heart"></i></a>
      <div class="card-heading">${sanPham.ten}</div>
      <div class="card-text">
        <p class="m-0">${sanPham.moTa}. Màn Hình: ${sanPham.manHinh}</p>
      </div>
      <div class="card-money">
        $ ${sanPham.gia}
        <span class="pl-5"
          >Số lượng:
          <div class="text-danger">
            <a
              class="btn btn-outline-info rounded-circle"
              onclick="qtyChange(this,'down')"
            >
              <i class="fa fa-angle-left text-black"></i>
            </a>
            <span style="padding: 0 5px; width: 20px" id="soLuongSP">0</span>
            <a
              class="btn btn-outline-info rounded-circle"
              onclick="qtyChange(this,'up')"
            >
              <i class="fa fa-angle-right"></i>
            </a></div
        ></span>
      </div>
      <a onclick="addToCart(this,${sanPham.id})" class="card-button"
        >Add to Cart</a
      >
    </div>
  </div>`;
  });
  document.getElementById("listSanPham").innerHTML = contentHTML;
};

// tinh tong san pham     /// total qty
export let tinhTongSP = (cart) => {
  let result = 0;
  cart.forEach((item) => {
    result += item.quantity * 1;
  });
  document.querySelector(".total-qty").innerText = result;
};

// render Cart
export let renderCart = (listSPMua) => {
  let contentHTML = "";
  listSPMua.forEach((sanPham) => {
    contentHTML += `<div class="col-2">
    <div class="card-image">
    <img src="${sanPham.product.hinhAnh}" /></div>
  </div>
  <div class="col-4">${sanPham.product.ten}</div>
  <div class="col-2">
    <span>
      <div class="text-danger d-block">
        <a
          class="btn btn-outline-info rounded-circle"
          onclick="qtyChangeCart(this,'down',${sanPham.product.id})"
        >
          <i class="fa fa-angle-left text-black"></i>
        </a>
        <span style="padding: 0 5px; width: 20px" id="soLuongSP"
          >${sanPham.quantity}</span
        >
        <a
          class="btn btn-outline-info rounded-circle"
          onclick="qtyChangeCart(this,'up',${sanPham.product.id})"
        >
          <i class="fa fa-angle-right"></i>
        </a></div
    ></span>
  </div>
  <div class="col-3">
    $ ${(sanPham.product.gia * sanPham.quantity).toLocaleString()} usd
  </div>
  <div class="col-1">
  <button class="btn btn-danger m-1" onclick="xoaSanPham(${
    sanPham.product.id
  })"><i class="fa fa-trash"></i>
  </button>
  </div>`;
  });
  document.getElementById("tableCartSP").innerHTML = contentHTML;
};

//tinh Tong tien thanh toan
export let tinhTongThanhToan = (cart) => {
  let result = 0;
  cart.forEach((item) => {
    result += item.product.gia * item.quantity;
  });
  return result.toLocaleString();
};

//

export class SanPham {
  constructor(
    id,
    ten,
    gia,
    manHinh,
    backCamera,
    frontCamera,
    hinhAnh,
    moTa,
    loai
  ) {
    this.id = id;
    this.ten = ten;
    this.gia = gia;
    this.manHinh = manHinh;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.hinhAnh = hinhAnh;
    this.moTa = moTa;
    this.loai = loai;
  }
}

// {
//     "name": "iphoneX",
//     "price": "1000",
//     "screen": "screen 68",
//     "backCamera": "2 camera 12 MP",
//     "frontCamera": "7 MP",
//     "img": "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
//     "desc": "Thiết kế mang tính đột phá",
//     "type": "iphone"
//     },
